#pragma once

/*
 * Template metaprogramming to create a sequence of integers
 * from 0 to N - 1.
 *
 * Can be replaced with the C++14 class std::integer_sequence
 * if the compiler supports it.
 *
 * References
 *     http://en.cppreference.com/w/cpp/utility/integer_sequence
 *     http://stackoverflow.com/a/12707595
 */

#include <type_traits>      // std::is_same

// TODO: Currently this creates a linear number of template instantiations. This means that
//       MakeIndexSequence<N> will stackoverflow when N > ~900. Could be reworked to be logrithmic.

// A simple sequence of non-negative numbers.
template<unsigned int... Indices>
struct IndexSequence
{

};

// Recursive case for generating a sequence.
template<unsigned int N, unsigned int... Indices>
struct MakeIndexSequence
{
	typedef typename MakeIndexSequence<N - 1, N - 1, Indices...>::Sequence Sequence;
};

// Base case for generating a sequence
template<unsigned int... Indices>
struct MakeIndexSequence<0, Indices...>
{
	typedef IndexSequence<Indices...> Sequence;
};

// Unit tests.
static_assert( std::is_same< MakeIndexSequence<0>::Sequence, IndexSequence<       > >::value, "MakeIndexSequence unit test failed!" );
static_assert( std::is_same< MakeIndexSequence<1>::Sequence, IndexSequence<0      > >::value, "MakeIndexSequence unit test failed!" );
static_assert( std::is_same< MakeIndexSequence<2>::Sequence, IndexSequence<0, 1   > >::value, "MakeIndexSequence unit test failed!" );
static_assert( std::is_same< MakeIndexSequence<3>::Sequence, IndexSequence<0, 1, 2> >::value, "MakeIndexSequence unit test failed!" );


