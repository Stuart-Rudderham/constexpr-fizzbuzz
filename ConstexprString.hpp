#pragma once

/*
 * A nul-terminated string class that supports some compile-time
 * operations and manipulations.
 *
 * References
 *     http://sourceforge.net/p/constexprstr/code/HEAD/tree/no-pp-constexpr_string.cpp
 */

#include "IndexSequence.hpp"
#include <iostream>             // std::cout, std::endl

// Helper function to check for equality between two char[]
//
// Equivalent to:
/*
if( N != M ) {
    return false;
} else {
    if( i >= N ) {
        return true;
    } else {
        if( str1[i] == str2[i] ) {
            return CharArrayEqual(str1, str2, i + 1);
        } else {
            return false;
        }
    }
}
*/
template<unsigned int N, unsigned int M>
constexpr
bool CharArrayEqual( const char(&str1)[N], const char(&str2)[M], unsigned int i = 0 )
{
    return N != M ? false : ( i >= N ? true : str1[i] == str2[i] && CharArrayEqual(str1, str2, i + 1) );
}

// Helper function to calcuate the length of a string.
template<unsigned int N>
constexpr
unsigned int Strlen( const char(&)[N] )
{
    // The "- 1" is because we ignore the terminating '\0' character
    return N - 1;
}


// Helper function to get the number of character necessary to encode
// an integral (i.e. non-floating point) number.
// This is necessary because we need to know how long the string is
// going to be before we create it.
template<int N>
constexpr
typename std::enable_if<(N >= 0 && N <= 9), unsigned int>::type
IntToStringLen()
{
    // One digit plus terminating '\0' character.
    return 2;
}

template<int N>
constexpr
typename std::enable_if<(N > 9), unsigned int>::type
IntToStringLen()
{
    return 1 + IntToStringLen<N / 10>();
}

template<int N>
constexpr
typename std::enable_if<(N < 0), unsigned int>::type
IntToStringLen()
{
    // Negative sign plus however long the positive number is.
    return 1 + IntToStringLen<-N>();
}

template<unsigned int N>
struct ConstexprString
{
    public:
        // Constructor.
        // Creates the string from a single char array.
        constexpr ConstexprString( const char(&str)[N] )
            : ConstexprString( str, typename MakeIndexSequence<N>::Sequence() )
        {

        }

        // Constructor.
        // Creates the string from the concatenation of two char arrays.
        template<unsigned int A, unsigned int B>
        constexpr ConstexprString( const char(&str1)[A], const char(&str2)[B])
            : ConstexprString( str1, typename MakeIndexSequence<A - 1>::Sequence(),
                               str2, typename MakeIndexSequence<B    >::Sequence() )
        {

        }

        // Constructor.
        // Creates a string out of a single character.
        constexpr ConstexprString( char c )
            : str{ c, '\0' }
        {
            static_assert( N == 2, "" );
        }

        // Get a character from the string.
        constexpr
        char operator[]( unsigned int i ) const
        {
            return i < N ? str[i] : throw "OutOfBounds";
        }

        // Get the length of the string (not including the terminating '\0' character).
        constexpr
        unsigned int Length() const
        {
            return N - 1;
        }

        // Printer.
        void Print() const
        {
            std::cout << str << std::endl;
        }



        template<unsigned int M>
        constexpr
        bool operator==( const char(&str1)[M] ) const
        {
            return CharArrayEqual(str, str1);
        }

        template<unsigned int M>
        constexpr
        bool operator!=( const char(&str1)[M] ) const
        {
            return !(*this == str1);
        }

    public:
        // Generic ctor.
        // Given a nul-terminated c-string of length N (including the ending NUL char) and an
        // arbitrary IntegerSequence of length N initialze the member variable using the indicies.
        template<unsigned int... Indices>
        constexpr ConstexprString(const char(&arr)[N], IndexSequence<Indices...>)
            : str{ arr[Indices]... }
        {
            static_assert(sizeof...(Indices) >= N, "Not enough indicies!");
            static_assert(sizeof...(Indices) <= N, "Too many indicies!");

            // TODO: Could add validation to IntegerSeqence to make sure all
            // the indicies are within range.
        }

        template<unsigned int A, unsigned int B, unsigned int... IndicesA, unsigned int... IndicesB>
        constexpr ConstexprString( const char(&strA)[A], IndexSequence<IndicesA...>,
                                   const char(&strB)[B], IndexSequence<IndicesB...> )
            : str{ strA[IndicesA]..., strB[IndicesB]... }
        {
            static_assert(A + B - 1 == N, "Size mismatch!");

            static_assert(sizeof...(IndicesA) == A - 1, "Size mismatch!");
            static_assert(sizeof...(IndicesB) == B, "Size mismatch!");

            // TODO: Could add validation to IntegerSeqence to make sure all
            // the indicies are within range.
        }

        // The internal char array.
        const char str[N];
};

// Return the concatenation of two ConstexprStrings.
template<unsigned int N, unsigned int M>
constexpr
ConstexprString<N + M - 1> operator+( const ConstexprString<N>& s1, const ConstexprString<M>& s2 )
{
    return ConstexprString<N + M - 1>( s1.str, s2.str );
}

// Equality check for two ConstexprStrings.
template<unsigned int N, unsigned int M>
constexpr
bool operator==( const ConstexprString<N>& s1, const ConstexprString<M>& s2 )
{
    return CharArrayEqual( s1.str, s2.str );
}

// Convenience function for creating a ConstexprString from a compile time char[].
template<unsigned int N>
constexpr
ConstexprString<N> CreateConstexprString( const char (&format)[N] )
{
    return ConstexprString<N>( format );
}

// Convenience functions for creating a ConstexprString from an integer.
template<int N>
constexpr
typename std::enable_if<(N >= 0), ConstexprString<IntToStringLen<N>()> >::type
CreateConstexprString()
{
    return CreateConstexprString<N / 10>() + CreateConstexprString<N % 10>();
}

template<int N>
constexpr
typename std::enable_if<(N < 0), ConstexprString<IntToStringLen<N>()> >::type
CreateConstexprString()
{
    return CreateConstexprString("-") + CreateConstexprString<-N>();
}

template<> constexpr ConstexprString<IntToStringLen<0>()> CreateConstexprString<0>() { return CreateConstexprString("0"); }
template<> constexpr ConstexprString<IntToStringLen<1>()> CreateConstexprString<1>() { return CreateConstexprString("1"); }
template<> constexpr ConstexprString<IntToStringLen<2>()> CreateConstexprString<2>() { return CreateConstexprString("2"); }
template<> constexpr ConstexprString<IntToStringLen<3>()> CreateConstexprString<3>() { return CreateConstexprString("3"); }
template<> constexpr ConstexprString<IntToStringLen<4>()> CreateConstexprString<4>() { return CreateConstexprString("4"); }
template<> constexpr ConstexprString<IntToStringLen<5>()> CreateConstexprString<5>() { return CreateConstexprString("5"); }
template<> constexpr ConstexprString<IntToStringLen<6>()> CreateConstexprString<6>() { return CreateConstexprString("6"); }
template<> constexpr ConstexprString<IntToStringLen<7>()> CreateConstexprString<7>() { return CreateConstexprString("7"); }
template<> constexpr ConstexprString<IntToStringLen<8>()> CreateConstexprString<8>() { return CreateConstexprString("8"); }
template<> constexpr ConstexprString<IntToStringLen<9>()> CreateConstexprString<9>() { return CreateConstexprString("9"); }

// Unit tests.
static_assert( CreateConstexprString("abc")[0] == 'a' , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("abc")[1] == 'b' , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("abc")[2] == 'c' , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("abc")[3] == '\0', "CreateConstexprString unit test failed!" );

static_assert( CreateConstexprString("")    == ""   , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("Foo") == "Foo", "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("Foo") != "Bar", "CreateConstexprString unit test failed!" );

static_assert( CreateConstexprString<0>()    == "0"   , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString<-0>()   == "0"   , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString<-10>()  == "-10" , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString<10>()   != "-10" , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString<5>()    == "5"   , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString<9999>() == "9999", "CreateConstexprString unit test failed!" );

static_assert( CreateConstexprString("")    + CreateConstexprString("")    == ""                             , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("")    + CreateConstexprString("")    == CreateConstexprString("")      , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("a")   + CreateConstexprString("")    == "a"                            , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("")    + CreateConstexprString("a")   == "a"                            , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("a")   + CreateConstexprString("b")   == "ab"                           , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("Foo") + CreateConstexprString("Bar") == "FooBar"                       , "CreateConstexprString unit test failed!" );
static_assert( CreateConstexprString("Foo") + CreateConstexprString("Bar") == CreateConstexprString("FooBar"), "CreateConstexprString unit test failed!" );

static_assert( CharArrayEqual(""       , ""       ) == true , "CharArrayEqual unit test failed!" );
static_assert( CharArrayEqual("a"      , ""       ) == false, "CharArrayEqual unit test failed!" );
static_assert( CharArrayEqual(""       , "a"      ) == false, "CharArrayEqual unit test failed!" );
static_assert( CharArrayEqual("a"      , "a"      ) == true , "CharArrayEqual unit test failed!" );
static_assert( CharArrayEqual("a"      , "b"      ) == false, "CharArrayEqual unit test failed!" );
static_assert( CharArrayEqual("FooBar" , "FooBar" ) == true , "CharArrayEqual unit test failed!" );
static_assert( CharArrayEqual("FooBar1", "FooBar2") == false, "CharArrayEqual unit test failed!" );
static_assert( CharArrayEqual("1FooBar", "2FooBar") == false, "CharArrayEqual unit test failed!" );

static_assert( Strlen(""   ) == 0u, "Strlen() unit test failed!" );
static_assert( Strlen("A"  ) == 1u, "Strlen() unit test failed!" );
static_assert( Strlen("AB" ) == 2u, "Strlen() unit test failed!" );
static_assert( Strlen("ABC") == 3u, "Strlen() unit test failed!" );

static_assert( IntToStringLen<-101>() == 5, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<-100>() == 5, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<- 99>() == 4, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<- 11>() == 4, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<- 10>() == 4, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<-  9>() == 3, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<-  1>() == 3, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<-  0>() == 2, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+  0>() == 2, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+  1>() == 2, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+  9>() == 2, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+ 10>() == 3, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+ 11>() == 3, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+ 99>() == 3, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+100>() == 4, "IntToStringLen unit test failed!" );
static_assert( IntToStringLen<+101>() == 4, "IntToStringLen unit test failed!" );
