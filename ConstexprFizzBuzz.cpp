/*
 * FizzBuzz.
 *
 * Write a program that prints the numbers from 1 to 100. But for
 * multiples of 3 print "Fizz" instead of the number and for
 * the multiples of 5 print "Buzz". For numbers which are multiples
 * of both 3 and 5 print "FizzBuzz".
 */

#include "ConstexprString.hpp"
#include <type_traits>              // std::enable_if
#include <iostream>                 // std::cout

// User can specifiy how long the sequence is through the command line.
#ifndef FIZZBUZZ_SEQUENCE_LENGTH
    #define FIZZBUZZ_SEQUENCE_LENGTH 100
#endif

/*
 * Helper functions for calculating the length of the constructed FizzBuzz string.
 */

// Forward declarations.
template<unsigned int N> constexpr typename std::enable_if<(N % 5 != 0) && (N % 3 != 0), unsigned int>::type FizzBuzzStrLen();
template<unsigned int N> constexpr typename std::enable_if<(N % 5 != 0) && (N % 3 == 0), unsigned int>::type FizzBuzzStrLen();
template<unsigned int N> constexpr typename std::enable_if<(N % 5 == 0) && (N % 3 != 0), unsigned int>::type FizzBuzzStrLen();
template<unsigned int N> constexpr typename std::enable_if<(N % 5 == 0) && (N % 3 == 0), unsigned int>::type FizzBuzzStrLen();

// Case 1: N is not divisible by 3 and is not divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 != 0) && (N % 3 != 0), unsigned int>::type
FizzBuzzStrLen()
{
    // Have the "- 1" because we remove the NUL character at the end of
    // the integer string.
    return Strlen(", ") - 1 + IntToStringLen<N>() + FizzBuzzStrLen<N - 1>();
}

// Case 2: N is divisible by 3 and is not divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 != 0) && (N % 3 == 0), unsigned int>::type
FizzBuzzStrLen()
{
    return Strlen("Fizz") + Strlen(", ") + FizzBuzzStrLen<N - 1>();
}

// Case 3: N is not divisible by 3 and is divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 == 0) && (N % 3 != 0), unsigned int>::type
FizzBuzzStrLen()
{
    return Strlen("Buzz") + Strlen(", ") + FizzBuzzStrLen<N - 1>();
}

// Case 4: N is divisible by 3 and is divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 == 0) && (N % 3 == 0), unsigned int>::type
FizzBuzzStrLen()
{
    return Strlen("FizzBuzz") + Strlen(", ") + FizzBuzzStrLen<N - 1>();
}

// Base Case: N is 0; stop recursing.
template<>
constexpr
unsigned int
FizzBuzzStrLen<1>()
{
    // The character '1' plus the terminating '\0' character
    return 2;
}

// Forward declarations.
template<unsigned int N> constexpr typename std::enable_if<(N % 5 != 0) && (N % 3 != 0), ConstexprString<FizzBuzzStrLen<N>()>>::type FizzBuzz();
template<unsigned int N> constexpr typename std::enable_if<(N % 5 != 0) && (N % 3 == 0), ConstexprString<FizzBuzzStrLen<N>()>>::type FizzBuzz();
template<unsigned int N> constexpr typename std::enable_if<(N % 5 == 0) && (N % 3 != 0), ConstexprString<FizzBuzzStrLen<N>()>>::type FizzBuzz();
template<unsigned int N> constexpr typename std::enable_if<(N % 5 == 0) && (N % 3 == 0), ConstexprString<FizzBuzzStrLen<N>()>>::type FizzBuzz();

// Case 1: N is not divisible by 3 and is not divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 != 0) && (N % 3 != 0), ConstexprString<FizzBuzzStrLen<N>()>>::type
FizzBuzz()
{
    return FizzBuzz<N - 1>() + CreateConstexprString(", ") + CreateConstexprString<N>();
}

// Case 2: N is divisible by 3 and is not divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 != 0) && (N % 3 == 0), ConstexprString<FizzBuzzStrLen<N>()>>::type
FizzBuzz()
{
    return FizzBuzz<N - 1>() + CreateConstexprString(", Fizz");
}

// Case 3: N is not divisible by 3 and is divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 == 0) && (N % 3 != 0), ConstexprString<FizzBuzzStrLen<N>()>>::type
FizzBuzz()
{
    return FizzBuzz<N - 1>() + CreateConstexprString(", Buzz");
}

// Case 4: N is divisible by 3 and is divisible by 5.
template<unsigned int N>
constexpr
typename std::enable_if<(N % 5 == 0) && (N % 3 == 0), ConstexprString<FizzBuzzStrLen<N>()>>::type
FizzBuzz()
{
    return FizzBuzz<N - 1>() + CreateConstexprString(", FizzBuzz");
}

// Base Case: N is 0; stop recursing.
template<>
constexpr
ConstexprString<FizzBuzzStrLen<1>()>
FizzBuzz<1>()
{
    return CreateConstexprString("1");
}

/*
 * Driver.
 */
constexpr static auto FizzBuzzString = FizzBuzz<FIZZBUZZ_SEQUENCE_LENGTH>();

int main()
{
    static_assert( FizzBuzzString[0] == '1', "Sanity Check" );

	return 0;
}
